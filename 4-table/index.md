# Tabellen
---

## Die Umgebungen `tabular` und `table`

Die `tabular`-Umgebung definiert eine Tabelle und benötigt kein spezielles Paket. Einzelne Einträge werden durch das Et-Zeichen `&` abgetrennt. Am Zeilenende muss mit Hilfe des Befehls `\\` eine neue Zeile gestartet werden.

Tabellen sollten aber – analog zur `figure`-Umgebung bei den Bildern – in eine `table`-Umgebung gesetzt werden. So kann auch hier eine `\caption` (Beschriftung) sowie ein Tabellenverzeichnis (mittels `\listoftables`) hinzugefügt werden. Wird die `table`-Umgebung weggelassen, so wird die Tabelle genau an der Stelle erscheinen, an der sie im Code steht.

``` latex
\begin{table}[h]
  \centering
  \begin{tabular}{lcr}
    \textbf{Verkehrsmittel} & \textbf{Gewicht} & \textbf{Geschwindigkeit} \\
    Auto & 1000kg & 100 km/h \\
    Fahrrad & 10kg & 20 km/h \\
  \end{tabular}
  \caption{Beschriftung}
\end{table}
```

Dies ergibt die folgende Tabelle:

![](./table-1.png)

Die Angabe in den geschweiften Klammer (`lcr`) zu Beginn der Umgebung definiert die Ausrichtung des Textes innerhalb der einzelnen Spalten. Die erste Spalte wird folglich links ausgerichtet, die zweite Spalte zentriert und die dritte rechtsbündig dargestellt.

Die erste Zeile wird mit Hilfe von `\textbf{fett}` anders formatiert, so dass sie als Titelzeile dient.

### Linien in Tabellen
Mit Hilfe des Befehls `hline{}` können waagrechte Linien eingefügt werden. Senkrechte Linien sind noch einfacher einzufügen, sie können nämlich in Form eines senkrechten Strichs `|`[^2] bei der Beschreibung der Spaltenausrichtung angegeben werden:

``` latex
\begin{table}[h]
  \centering
  \begin{tabular}{|l||cr|}
    \hline
    \textbf{Verkehrsmittel} & \textbf{Gewicht} & \textbf{Geschwindigkeit} \\
    \hline\hline
    Auto & 1000kg & 100 km/h \\
    Fahrrad & 10kg & 20 km/h \\
    \hline
  \end{tabular}
  \caption{Beschriftung}
\end{table}
```

![](./table-2.png)


### Die Gesamtbreite der Tabelle ändern

Die `tabular`-Umgebung kennt auch noch eine `*`-Variante. Wird diese Form verwendet, kann über einen zusätzlichen Parameter die Gesamtbreite der Tabelle beeinflusst werden:

``` latex
\begin{table}[h]
  \centering
    \begin{tabular*}{15cm}{@{\extracolsep\fill}l|cr}
    \textbf{Verkehrsmittel} & \textbf{Gewicht} & \textbf{Geschwindigkeit} \\
    \hline
    Auto & 1000kg & 100 km/h \\
    Fahrrad & 10kg & 20 km/h \\
    \end{tabular*}
  \caption{Beschriftung}
\end{table}
```

Der zusätzliche Eintrag `@{\extracolsep\fill}` fügt in den nachfolgenden Spalten zusätzlichen Zwischenraum ein. Anstelle von `\fill` kann auch ein bestimmter Wert (z.B. `{5cm}`) eingetragen werden. In Kombination mit der `*`-Variante macht aber nur `\fill` Sinn, damit die angegebene Breite genau erreicht wird.

![](./table-3.png)

### Mehrzeiliger Text in einer Spalte

Neben den bereits erwähnten Ausrichtungszeichen `l`, `c` und `r` kann auch noch `p{längenangabe}` verwendet werden.

![](./table-4.png)

::: warning Achtung
Innerhalb einer Tabellenzelle, die umgebrochen wird, darf der Befehl `\\` **nicht** auftreten, sonst wird die ganze Tabellenzeile beendet. `\linebreak` und `\newline` sind hingegen erlaubt.
:::


## Das Paket `booktabs`

Das Paket `booktabs`[^1] verbessert die Darstellung von Tabellen. Einerseits fügt dieses Paket zusätzlichen Leerraum oberhalb und unterhalb von horizontalen Linien ein. Andererseits definiert es Befehle für horizontale Linien unterschiedlicher Stärke.

Die zur Verfügung stehenden Befehle sind `\toprule`, `\midrule` und `\bottomrule`. Der `\midrule`-Befehl ist gedacht, um die Überschriftzeile vom Rest der Tabelle zu trennen. Wenn `@{}` zu Beginn und am Ende des `tabular`-Parameters zur Ausrichtung der Zelleninhalte angeben wird, so werden die Linien nicht breiter als der Tabelleninhalt selbst, was besser aussieht, wenn ausschliesslich horizontale Linien verwendet werden. Zudem gibt es den Befehl `\addlinespace`, der es ermöglicht, z.B. vor einer Totalzeile zusätzlichen Leerraum einzufügen.

::: warning
Tabellen mit vertikalen Linien und doppelte Linien werden in guten wissenschaftlichen Publikationen nicht mehr verwendet. Es ist moderner, nur horizontale Linien einzusetzen.
:::

``` latex
\begin{table}[h]
  \centering
  \begin{tabular}{@{}lcr@{}}
    \toprule
    \textbf{Verkehrsmittel} & \textbf{Gewicht} & \textbf{Geschwindigkeit} \\
    \midrule
    Auto & 1000kg & 100 km/h \\
    Fahrrad & 10kg & 20 km/h \\
    \bottomrule
  \end{tabular}
  \caption{Beschriftung}
\end{table}
```

![](./table-5.png)


[^1]: [Anleitung zum Paket Booktabs][1]
[^2]: Ein senkrechter Strich `|` kann mit [Alt Gr]+[7] eingegeben werden.

[1]: http://texcatalogue.sarovar.org/macros/latex/contrib/booktabs/booktabs.pdf
