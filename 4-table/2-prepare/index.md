# Tabellen vorbereiten
---

Wenn man Tabellen von Excel oder einer Webseite kopiert, fehlen die Spalten- und Zeilentrennzeichen `&` und `\\`.

Diese können in Word automatisch eingefügt werden. Wenn man eine Tabelle als Text in Word einfügt, so sind die Spalten durch ein Tabulatorzeichen und die Zeilen durch einen Absatzwechsel getrennt. Mit Hilfe von _Suchen und Ersetzen_ werden diese anschliessend durch die entsprechenden Trennzeichen für LaTeX ersetzt.

Dazu geht man folgendermassen vor:

1. Tabelle als Text in Word einfügen.

2. Alle Tabulatorzeichen durch `&` ersetzen. Suche nach `^t`, ersetzen durch ` & `.

3. Alle Absatzwechsel mit `\\` ergänzen. Suche nach `^p`, ersetzen durch `\\^p`.
