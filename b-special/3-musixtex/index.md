# Musik mit musixtex
---

::: warning TODO
- Endstriche
:::

Mit dem Paket `musixtex` können professionell aussehende Notenblätter gesetzt werden.

``` latex
\usepackage{musixtex}
```

## Umgebung

Ein Musikstück wird immer in eine `music`-Umgebung gesetzt:

::: columns 2
``` latex
\begin{music}
\startextract
\endextract
\end{music}
```

***
![](./example-1.png)
:::

Mit `\startextract` wird eine Notenlinie begonnen, mit `\endextract` wird diese beendet.

## Noten

Noten werden immer mit dem Befehl `\Notes` eingeleitet und mit `\en` beendet.

Noten werden mit Befehlen geschrieben. Der Befehl gibt den Notenwert an, als Argument wird die Tonhöhe angegeben. Die Befehlsvariante mit p erzeugt eine gepunktete Note.

| Notenwert   | Befehl   | gepunktet |
| ----------- | -------- | --------- |
| Ganze       | `\wh{}`  | `\hwp{}`  |
| Halb        | `\ha{}`  | `\hap{}`  |
| Viertel     | `\qa{}`  | `\qap{}`  |
| Achtel      | `\ca{}`  | `\cap{}`  |
| Sechzehntel | `\cca{}` | `\ccap{}` |

Bei diesen Befehlen wird die Richtung des Notenhalses automatisch festgelegt. Wenn man diesen manuell kontrollieren will, verwendet man beispielsweise `\hl{}` (halbe Note mit Hals nach unten) und `\hu{}` (halbe Note mit Hals nach oben).

Die Tonhöhe wird mit den Buchstaben `a` bis `g` angegeben. Ein Apostroph `'` transponiert alle folgenden Noten um eine Oktave nach oben, ein ` transponiert nach unten.

Um eine Note zu erhöhen, stellt man ein Zirkumflex `^` voran, um sie zu vertiefen ein Unterstrich `_`.

::: columns 2
``` latex
\begin{music}
\startextract
\Notes
\qu{abcdefg} \cl{'c} \hu{`c}
\en
\endextract
\end{music}
```

***
![](./example-2.png)
:::

## Balken

Balken erhalten immer eine Nummer, da gleichzeitig mehrere Balken offen sein können. Um einen Balken zu erzeugen, muss folgende Sequenz beachtet werden:

1. Balken eröffnen
2. erste Note hinzufügen
2. weitere Noten hinzufügen
3. Balken abschliessen
4. letzte Note hinzufügen

### Balken eröffnen

Mit einem der folgenden Befehle wird ein Balken eröffnet:

| Art               | Balken oben | Balken unten |
| ----------------- | ----------- | ------------ |
| einfacher Balken  | `\ibu`      | `\ibl`       |
| doppelter Balken  | `\ibbu`     | `\ibbl`      |
| dreifacher Balken | `\ibbbu`    | `\ibbbl`     |
| vierfacher Balken | `\ibbbbu`   | `\ibbbbl`    |

Jeder dieser Befehle benötigt drei Argumente:

~~~ latex
\ibu{nummer}{note}{winkel}
~~~
`nummer` ist die Nummer des Balkens. `note` ist die erste Note. Diese Information wird nur benötigt, um die vertikale Position des Balkens festzulegen. Die Note selbst muss auch noch hinzugefügt werden. `winkel` gibt die Richtung des Balkens an. Für `winkel` können Werte zwischen -9 und 9 angegeben werden.

### Noten hinzufügen

Mit dem folgenden Befehl wird eine Note zu einem Balken hinzugefügt:

~~~ latex
\qb{nummer}{note}
~~~

### Balken beenden

Mit dem Befehl `\tbu` wird ein oberer Balken beendet, mit `\tbl` ein unterer Balken:

~~~ latex
\tbu{nummer}
~~~

### Beispiel

``` latex
\ibu{1}{'c}{2}\qb{1}{c}\qb{1}{d}\qb{1}{e}\tbu{1}\qb{1}{f}
```

![](./example-6.png)

## Taktstriche

Ein Taktstrich wird mit `\bar` erzeugt. Vorher müssen die aktuellen Noten mit `\en` beendet werden:

``` latex
\Notes \qu{abcd} \en \bar \Notes \qu{abcd} \en
```

## Notenschlüssel

Mit dem Befehl `\setclef{n}{123}` werden Notenschlüssel für das Instrument n festelegt.

## Tonart

Mit dem Befehl `\generalsignature{t}` wird die Tonart angegeben. Für `t` wird eine Zahl angegeben:

| `t` | Bedeutung |
| --- | --------- |
| -   | C-Dur     |
| 1   | G-Dur     |
| 2   | D-Dur     |
| -1  | d-Moll    |
| -2  | g-Moll    |

usw.

Beispielsweise wird mit `\generalsignature{2}` D-Dur gewählt.

::: columns 2
``` latex
\begin{music}
\generalsignature{2}
\startextract
\endextract
\end{music}
```
***
![](./example-3.png)
:::

## Takt

Mit dem Befehl `\generalmeter{takt}` wird der Takt festgelegt. Typische Werte für `takt` sind:

- `\meterC` für Viervierteltakt
- `\allabreve` für Allabreve
- `\meterfrac{3}{4}` beispielsweise für Dreivierteltakt

::: columns 2
``` latex
\begin{music}
\generalmeter{\meterfrac{3}{4}}
\startextract
\endextract
\end{music}
```
***
![](./example-4.png)
:::

## Pausen

Mit folgenden Befehlen werden Pausensymbole erzeugt:
| Dauer            | Befehl   |
|:---------------- |:-------- |
| ganze Pause      | `\pause` |
| halbe Pause      | `\hp`    |
| Viertelpause     | `\qp`    |
| Achtelpause      | `\ds`    |
| Sechzehntelpause | `\qs`    |

::: columns 2
``` latex
\begin{music}
\startextract
\Notes
\pause \hp \qp \ds \qs
\en
\endextract
\end{music}
```

***
![](./example-5.png)
:::

## Anleitung

Eine ausführliche Anleitung findet sich hier:

* [:pdf: Anleitung](https://mirror.foobar.to/CTAN/macros/musixtex/doc/musixdoc.pdf)
