# Musik mit harmony
---

Mit dem Paket `harmony` können einzelne Noten und weitere Symbole dargestellt werden:

``` latex
\usepackage{harmony}
```

## Takt

Ein Takt wird mit folgendem Befehl dargestellt:

``` latex
\Takt{3}{4}
```

## Noten

![](./notes-1.png)

## Zusammengesetzte Noten

Mit den folgenden Befehlen können zusammengesetzte Noten erzeugt werden:

![](./notes-2.png)

## Beispiele

![](./notes-3.png)

## Weitere Notationen

![](./harmony-1.png)

![](./harmony-2.png)
