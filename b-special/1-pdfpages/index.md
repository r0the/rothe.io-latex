# PDFs einbinden
---

Mit dem Paket `pdfpages` können Seiten aus einem PDF-Dokument in ein LaTeX-Dokument eingebunden werden. Dazu wird wie üblich erst das Paket geladen:

``` latex
\usepackage{pdfpages}
```

## Ganzes Dokument

Mit dem folgenden Befehl wird ein ganzes PDF-Dokument in das LaTeX-Dokument eingefügt:

``` latex
\includepdf[pages=-]{datei.pdf}
```

## Einzelne Seiten

Mit dem folgenden Befehl werden die Seiten 1 und 3 aus dem PDF-Dokument eingefügt:
``` latex
\includepdf[pages={1,3}]{datei.pdf}
```
