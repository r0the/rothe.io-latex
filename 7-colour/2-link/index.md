# 7.2 Links
---

Um Links in einem LaTeX-Dokument schön darzustellen, wird das Paket `hyperref` verwendet. Es wird wie üblich eingebunden mit:

``` latex
\usepackage{hyperref}
```

Das Einbinden des Pakets hat den Effekt, dass alle Einträge im Inhaltsverzeichnis im PDF zu Links werden.

Links werden normalerweise mit einem roten Rahmen dargestellt. Da dies nicht besonders toll aussieht, will man die Farbgebung normalerweise anpassen.

## Links Einfügen

Um einen Link im Text einzufügen wird die Anweisung `\url` verwendet:

``` latex
Auf der Website \url{https://www.gymkirchenfeld.ch} finden
Sie weitere Informationen.
```

## Einstellungen

Mit der Anweisung `\hypersetup` kann die Darstellung der Links konfiguriert werden:

``` latex
\hypersetup{breaklinks=true, pdfborder={0 0 0}}
```

Mögliche Einstellungen sind:

| Einstellung       | Standardwert | Beschreibung                      |
| ----------------- | ------------ | --------------------------------- |
| `breaklinks`      | `false`      | lange Links am Zeilenende trennen |
| `colorlinks`      | `false`      | Links farbig darstellen           |
| `linkbordercolor` | `{1 0 0}`    | Farbe der Link-Umrahmung in RGB   |
