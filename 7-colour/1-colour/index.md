# 7.1 Farben
---

Mit dem Paket `xcolor` kann man Farbe von Schrift und Hintergrund ändern. Das Paket wird mit

``` latex
\usepackage{xcolor}
```

eingebunden.

## Vordefinierte Farben

Die folgenden Farben stehen immer zu Verfügung:

![](./colour.png)

## Farben definieren

~~~ latex
\definecolor{<mark>Farbe</mark>}{rgb}{<mark>r</mark>,<mark>g</mark>,<mark>b</mark>  }
~~~
definiert eine neue Farbe. Dabei ist `Farbe` der Name der Farbe. `r`, `g` und `b` sind die RGB-Werte der Farbe. Sie müssen zwischen 0 und 1 liegen.

Der folgende Befehl definiert die Farbe `Hellblau`:

``` latex
\definecolor{Hellblau}{rgb}{0.65, 0.81, 1}
```

Mit dem untenstehenden Tool können gewünsche Farbwerte ausgewählt werden:

<VueColourPicker percent output="tuple"/>

## Farben verwenden

### Seitenhintergrund

~~~ latex
\pagecolor{<mark>Farbe</mark>}
~~~
legt die Hintergrundfarbe fest.

### Text

~~~ latex
\textcolor{<mark>Farbe</mark>}{<mark>Text</mark>}
~~~
legt die Textfarbe für den angegebenen Text fest.

### Hintergrund

~~~ latex
\colorbox{<mark>Farbe</mark>}{<mark>Text</mark>}
~~~
legt die Hintergrundfarbe für den angegebenen Text fest.

### Rahmen

~~~ latex
\fcolorbox{<mark>Rahmenfarbe</mark>}{<mark>Hintergrundfarbe</mark>}{<mark>Text</mark>}
~~~
legt die Rahmen- und Hintergrundfarbe für den angegebenen Text fest.

``` latex
\fcolorbox{red}{white}{Text}
```
