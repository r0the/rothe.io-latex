# 3.3 Massangaben
---

Bei einigen LaTeX-Befehlen kann oder muss eine Länge oder Höhe angegeben werden.

Beim Festlegen von Abständen können folgende Massangaben verwendet werden:

| Einheit | Bedeutung                                                |
|:------- |:-------------------------------------------------------- |
| `cm`    | Zentimeter                                               |
| `mm`    | Millimeter                                               |
| `in`    | Inches (Zoll = 2.54 cm)                                  |
| `pt`    | Punkte (1 in = 72.27 pt)                                 |
| `em`    | Die Breite des Geviertstrichs --- im aktiven Zeichensatz |
| `ex`    | Die Höhe des Buchstabens x im aktiven Zeichensatz        |

Die Masse werden als ganze Zahl oder Dezimalzahl mit direkt angefügter Masseinheit angegeben, also z.B. `2.5cm`.

## Vordefinierte Längen

LaTeX stellt einige Befehle für von LaTeX verwendete Längen zu Verfügung:

| Befehl         | Bedetung                    |
|:-------------- |:--------------------------- |
| `\textwidth`   | Breite des Textbereiches    |
| `\columnwidth` | Breite der aktuellen Spalte |
| `\linewidth`   | Breite der aktuellen Zeile  |

Diese können mit einem Faktor multipliziert werden. So wird ein Viertel der Seitenbreite mit folgendem Befehl berechnet:

``` latex
0.25\textwidth
```
