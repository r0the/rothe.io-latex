# 3.4 Positionierung
---

Man braucht sich nicht um die Positionierung zu kümmern, das macht LaTeX automatisch. Allerdings gibt es die Möglichkeit, die Positionierung zu beeinflussen. Die `figure`-Umgebung kennt optionale Parameter, die LaTeX mitteilen, wo das Bild bevorzugt stehen soll:

| Option | Abkürzung für    | Bedeutung                                       |
|:------ |:---------------- |:----------------------------------------------- |
| `h`    | *here*           | wenn möglich an der aktuellen Stelle            |
| `t`    | *top*            | oben an der Seite                               |
| `b`    | *bottom*         | unten an der Seite                              |
| `p`    | *page of floats* | mit anderen Bildern auf einer eigenen Seite     |
| `H`    | *here*           | immer an der aktuellen Stelle (nur mit `float`) |

Die oben erklärten Positionsbeschreibungen können auch kombiniert werden, da es sich ja um Empfehlungen und nicht um strikte Vorgaben handelt:

``` latex
\begin{figure}[h]
  \centering
  \includegraphics[height=7cm]{Bild.png}
  \caption{Bild-Beschreibung}
\end{figure}
```

Falls LaTeX die Positionierung an der aktuellen Stelle ungünstig erscheint, wird es mit anderen Bildern auf eine eigene Seite platziert. Zudem wird es auf eine Höhe von 7 cm skaliert. Der Befehl `\caption` zeigt unterhalb des Bildes den angegebenen Text an. Der Befehl `\centering` zentriert das Bild.

## Das Paket `float`

Mit der Positionierung `h` ist nicht garantiert, dass eine Abbildung genau an dieser Stelle zu stehen kommt. Beispielsweise wird eine Abbildung nie direkt nach einer Überschrift positioniert. Um eine Positionierung zu erzwingen, kann das Zusatzpaket `float` eingebunden werden. Es definiert die zusätzliche Positionierung `H`, welche bewirkt, dass die Abbildung auf jeden Fall an dieser Stelle dargestellt wird.

``` latex
\usepackage{float}
```

## Das Paket `subcaption`

Mit dem Paket `subfig` können in einer `figure`-Umgebung mehrere Bilder nebeneinander platziert werden. Erst wird das Paket mit dem folgenden Befehl eingebunden:

``` latex
\usepackage{subcaption}
```

Nun kann die Umgebung `subfigure` verwendet werden, um mehrere Bilder in einer `figure`-Umgebung zu platzieren:

``` latex
\begin{figure}[h]
  \centering
  \begin{subfigure}[B]{0.49\textwidth}
    \centering
    \includegraphics[width=0.4\textwidth]{katze.pdf}
    \caption{Eine Katze}
  \end{subfigure}
  \hfill
  \begin{subfigure}[B]{0.49\textwidth}
    \centering
    \includegraphics[width=\textwidth]{elefant.pdf}
    \caption{Ein Elefant}
  \end{subfigure}
  \caption{Tiere}
\end{figure}
```

In jeder `subfigure`-Umgebung kann eine Beschreibung für das entsprechende Bild eingefügt werden. Der `\hfill`-Befehl sorgt dafür, dass zwischen den Bildern der maximal mögliche Abstand eingefügt wird.

Mit dem optionalen Parameter (im Beispiel `B`) wird die vertikale Ausrichtung der einzelnen Bilder angegeben. Folgende Werte sind möglich:

| Option | Abkürzung für | Bedeutung                           |
|:------ |:------------- |:----------------------------------- |
| `B`    | *bottom*      | Bilder am unteren Rand ausgerichtet |
| `c`    | *center*      | vertikal zentriert (Standard)       |
| `T`    | *top*         | Bilder am oberen Rand ausgerichtet  |

::: columns 3
![subfigure mit B](./subfigure-bottom.png)
***
![subfigure mit c](./subfigure-center.png)
***
![subfigure mit T](./subfigure-top.png)
:::

## Das Paket `wrapfig`

Das Paket `wrapfig` ermöglicht das Einfügen von Bildern, die vom Text umflossen werden. Dass das Bild am rechten Seitenrand stehen soll, spezifiziert die Option `r` für die `wrapfigure`-Umgebung. Das Bild soll 50% der Textbreite einnehmen, was mit der Breitenangabe `0.5\textwidth` erreicht wird.

``` latex
\begin{wrapfigure}{r}{0.5\textwidth}
  \centering
  \includegraphics[width=0.45\textwidth]{Bild.png}
  \caption{Beschriftung}
\end{wrapfigure}
```

Anstelle von `r` können diverse andere Werte verwendet werden:

| Option | Abkürzung für | Bedeutung                     |
|:------ |:------------- |:----------------------------- |
| `l`    | *left*        | Bild wird links positioniert  |
| `r`    | *right*       | rechts                        |
| `i`    | *inner*       | innen (für `twoside`-Layout)  |
| `o`    | *outer*       | aussen (für `twoside`-Layout) |

Die `wrapfigure`-Umgebung kennt noch optionale Parameter, die die Positionierung detaillierter beeinflussen:

``` latex
\begin{wrapfigure}[5]{r}[1cm]{0.5\textwidth}
  \centering
  \includegraphics[width=0.45\textwidth]{Bild.png}
\end{wrapfigure}
```

Die erste Option (im Beispiel die Zahl `5`) teilt LaTeX mit, dass neben dem Bild 5 Zeilen stehen sollen. Wird die Option weggelassen, werden die Anzahl Zeilen automatisch errechnet. Die zweite Option (hier `1cm`) definiert den sogenannten *Randüberhang*, d.h. eine Längenangabe, die angibt, wie weit das Bild über den Rand hinausragt.

::: warning Achtung
Der Text, der die Abbildung umfliessen soll, muss **nach** der `wrapfigure`-Umgebung stehen.
:::

Die `wrapfigure`-Umgebung lässt oberhalb des Bildes eine Zeile Abstand. Wenn das Bild bündig zur ersten Textzeile ausgerichtet werden soll, muss in der `wrapfigure`-Umgebung ein negativer Abstand von einer Zeile eingefügt werden.

``` latex
\begin{wrapfigure}{r}{0.5\textwidth}
  \vspace{-\baselineskip}
  \centering
  \includegraphics[width=0.45\textwidth]{Bild.png}
\end{wrapfigure}
```
