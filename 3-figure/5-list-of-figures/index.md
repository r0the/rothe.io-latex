# Abbildungsverzeichnis
---

Längere Texte enthalten typischerweise ein Abbildungsverzeichnis. Um das Abbildungsverzeichnis zu erstellen, wird der folgende Befehl verwendet:

~~~ latex
\listoffigures
~~~
fügt ein Abbildungsverzeichnis ein.

Im Abbildungsverzeichnis erscheinen alle Bilder, bei welchen mit dem `\caption{}`-Befehl eine Beschriftung definiert wurde.

## Beschriftung

~~~ latex
\caption{Beschriftung}
~~~
legt eine **Beschriftung** für ein Bild fest. Der Befehl muss innerhalb der `figure`-Umgebung stehen. Die Beschriftung erscheint ebenfalls im Abbildungsverzeichnis.

``` latex
\begin{figure}
  \centering
  \includegraphics[width=10cm]{bilder/bild-titel.jpg}
  \caption{Hier steht die Bildbeschriftung}
\end{figure}
```

~~~ latex
\caption[Verzeichniseintrag]{Beschriftung}
~~~
legt eine **Beschriftung** für ein Bild fest. Im Abbildungsverzeichnis erscheint der **Verzeichniseintrag**.

``` latex
\begin{figure}
  \centering
  \includegraphics[width=10cm]{bilder/bild-titel.jpg}
  \caption[Bildbeschreibung, Quelle: Wikimedia Commons, Autor: xyz]{Hier steht die Bildbeschriftung}
\end{figure}
```
