# 3.2 Abbildungen
---

Viele Textformen enthalten **Bilder** oder **Illustrationen**, welche nicht Teil des Textes sind. Sie werden auf der Seite abgetrennt vom Fliesstext positioniert. Solche Elemente nennen wird hier *Abbildungen* (engl. *figures*).

In LaTeX wird ein solches Element mit `\begin{figure}` eingeleitet und endet mit `\end{figure}`. Was dazwischen steht, wird nicht direkt an dieser Textstelle positioniert. LaTeX versucht, alle *figures* möglichst sinnvoll über die Seiten des Dokuments zu verteilen.

``` latex
\begin{figure}
  \includegraphics[width=10cm]{bilder/bild-titel.jpg}
  \caption{Hier steht die Bildbeschriftung}
\end{figure}
```

Um eine Abbildung zentriert auf der Seite darzustellen, kann zusätzlich der Befehl `\centering` verwendet werden:

``` latex
\begin{figure}
  \centering
  \includegraphics[width=10cm]{bilder/bild-titel.jpg}
  \caption{Hier steht die Bildbeschriftung}
\end{figure}
```

Zu einer Abbildung gehört immer eine **Beschriftung** (engl. *caption*), welche die Abbildung beschreibt. Die Beschriftung wird in LaTeX mit dem Befehl `\caption{}` definiert.
