# 3.1 Bilder
---

Um Grafikdateien in ein LaTeX-Dokument einbinden zu können, wird das Paket `graphicx` verwendet:

``` latex
\usepackage{graphicx}
```

Nun kann eine Bilddatei mit dem Befehl `\includegraphics{Datei}` an einer beliebigen Stelle der LaTeX-Dokuments eingefügt werden.

``` latex
\includegraphics{bilddatei.jpg}
```

 Üblicherweise werden die Bilddateien in einem **Unterordner** an der gleichen Stelle, wo sich das LaTeX-Dokument befindet, abgelegt. In unserem Beispiel heisst der Unterordner **bilder**. Der Unterordner muss dem Dateinamen vorangestellt werden:

 ``` latex
 \includegraphics{bilder/bilddatei.jpg}
 ```

Oft will man zusätzlich angeben, wie gross das Bild im Dokument dargestellt werden soll. Das geschieht, indem zwischen Befehl und Dateiname die gewünschte Breite oder Höhe des Bildes mit `width=` oder `height=` angegeben wird:

``` latex
\includegraphics[width=7cm]{bilder/bilddatei.jpg}
```

Es ist auch möglich, gleichzeitig Breite **und** Höhe des Bildes anzugeben, dies kann jedoch zu einer Verzerrung des Bildes führen:

``` latex
\includegraphics[width=7cm,height=1cm]{bilder/bilddatei.jpg}
```
