# 6.4 Matrizen und Klammern
----

## Matrizen

Matrizen können in der Mathematikumgebung mit Hilfe von Tabellen dargestellt werden. Die `array`-Umgebung verhält sich gleich wie die `tabular`-Umgebung.

$$
\begin{array}{cccc}
x_{11} & x_{12} & \cdots & x_{1n} \\
\vdots & \vdots & \ddots & \vdots \\
x_{n1} & x_{n2} & \cdots & x_{nn} \\
\end{array}
$$

Diese Darstellung erreicht man mit folgendem Code:

``` latex
\begin{equation*}
  \begin{array}{cccc}
    x_{11} & x_{12} & \cdots & x_{1n} \\
    \vdots & \vdots & \ddots & \vdots \\
    x_{n1} & x_{n2} & \cdots & x_{nn} \\
  \end{array}
\end{equation*}
```

Die Punkte erzeugt man mit den Befehlen `\ldots` (*low dots*), `\cdots` (*center dots*), `\vdots` (*vertical dots*) und `\ddots` (*diagonal dots*). Zudem gibt es den Befehl `\dotfill`, der eine Spalte mit Punkten auffüllt.

::: exercise Aufgabe
Bilde die folgende Matrix im Mathematik-Modus nach:

$$
\begin{array}{c@{\:+\:}c@{\;+\;}c@{+}c@{=}c}
a_{11}x_1 & a_{12}x_2 & \cdots & a_{1n}x_n & b_1 \\
a_{21}x_1 & a_{22}x_2 & \cdots & a_{2n}x_n & b_2 \\
\multicolumn{5}{c}{\dotfill} \\
a_{n1}x_1 & a_{n2}x_2 & \cdots & a_{nn}x_n & b_n \\
\end{array}
$$
:::
