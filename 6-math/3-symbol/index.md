# 6.3 Symbole
---

## Griechische Buchstaben

Die folgende Tabelle zeigt, wie griechische Buchstaben in LaTeX geschrieben werden. Für das kleine Omikron und einige Grossbuchstaben gibt es kein LaTeX-Befehl, es werden entsprechende lateinische Buchstaben verwendet. Die Kleinbuchstaben Epsilon, Theta, Rho und Phi werden als Variablen speziell dargestellt. Dafür gibt es die speziellen Befehle, welche mit `\var` beginnen.


| Buchstabe | klein                      | LaTeX                      | gross      | LaTeX      |
|:--------- |:-------------------------- |:-------------------------- |:---------- | ---------- |
| Alpha     | $\alpha$                   | `\alpha`                   | $A$        | `A`        |
| Beta      | $\beta$                    | `\beta`                    | $B$        | `B`        |
| Gamma     | $\gamma$                   | `\gamma`                   | $\Gamma$   | `\Gamma`   |
| Delta     | $\delta$                   | `\delta`                   | $\Delta$   | `\Delta`   |
| Epsilon   | $\epsilon$ / $\varepsilon$ | `\epsilon` / `\varepsilon` | $E$        | `\Epsilon` |
| Zeta      | $\zeta$                    | `\zeta`                    | $Z$        | `Z`        |
| Eta       | $\eta$                     | `\eta`                     | $H$        | `H`        |
| Theta     | $\theta$ / $\vartheta$     | `\theta` / `\vartheta`     | $\Theta$   | `\Theta`   |
| Iota      | $\iota$                    | `\iota`                    | $I$        | `I`        |
| Kappa     | $\kappa$                   | `\kappa`                   | $K$        | `K`        |
| Lambda    | $\lambda$                  | `\lambda`                  | $\Lambda$  | `\Lambda`  |
| Mu        | $\mu$                      | `\mu`                      | $M$        | `M`        |
| Nu        | $\nu$                      | `\nu`                      | $N$        | `N`        |
| Xi        | $\xi$                      | `\xi`                      | $\Xi$      | `\Xi`      |
| Omikron   | $o$                        | `o`                        | $O$        | `O`        |
| Pi        | $\pi$                      | `\pi`                      | $\Pi$      | `\Pi`      |
| Rho       | $\rho$ / $\varrho$         | `\rho` / `\varrho`         | $P$        | `P`        |
| Sigma     | $\sigma$                   | `\sigma`                   | $\Sigma$   | `\Sigma`   |
| Tau       | $\tau$                     | `\tau`                     | $T$        | `T`        |
| Upsilon   | $\upsilon$                 | `\upsilon`                 | $\Upsilon$ | `\Upsilon` |
| Phi       | $\phi$ / $\varphi$         | `\phi` / `\varphi`         | $\Phi$     | `\Phi`     |
| Chi       | $\chi$                     | `\chi`                     | $X$        | `X`        |
| Psi       | $\psi$                     | `\psi`                     | $\Psi$     | `\Psi`     |
| Omega     | $\omega$                   | `\omega`                   | $\Omega$   | `\Omega`   |

## Relationszeichen

In der nachfolgenden Tabelle finden Sie die am häufigsten verwendeten Relationszeichen. Es gibt aber zahlreiche weitere Relationszeichen, die hier der Einfachheit halber weggelassen wurden.

| Relation  | LaTeX     | Relation    | LaTeX       |
| --------- | --------- | ----------- | ----------- |
| $\le$     | `\leq`    | $\subset$   | `\subset`   |
| $\ge$     | `\geq`    | $\subseteq$ | `\subseteq` |
| $\ne$     | `\neq`    | $\supset$   | `\supset`   |
| $\ll$     | `\ll`     | $\supseteq$ | `\supseteq` |
| $\gg$     | `\gg`     | $\in$       | `\in`       |
| $\sim$    | `\sim`    | $\exists$   | `\exists`   |
| $\approx$ | `\approx` | $\forall$   | `\forall`   |

::: exercise Aufgabe
Stelle den Sinus- und den Kosinussatz korrekt dar.
:::
