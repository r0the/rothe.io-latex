# 6.1 Mathematik-Umgebungen
---

Für mathematische Formeln gibt es vier verschiedene Umgebungen. Einerseits kann die Formel innerhalb eines Abschnittes auftreten. Bei abgesetzten Formeln wird unterschieden, ob sie fortlaufend nummeriert werden sollen oder nicht. Zudem gibt es noch die Möglichkeit, eine ganze Liste von Formeln zu definieren.

Es ist wichtig zu wissen, dass Leerraum in Formeln unberücksichtigt bleibt. LaTeX wählt den Abstand zwischen Operatoren, Konstanten und Variablen automatisch. Die Konstanten und Variablen werden gemäss internationalem Standard dargestellt: Konstanten normal, Variablen kursiv.

Sämtliche Operationszeichen (`+` `-` `*` `/`), die Relationszeichen (`=` `<` `>`) Klammern (`(` `)` `[` `]`) sowie die folgenden weiteren Zeichen (`:` `!` `'` `|`) können direkt dargestellt werden. Sollen die geschweiften Klammern gedruckt werden, muss ihnen ein Backslash vorangestellt werden, weil sie als Befehlsklammern verwendet werden.

## Inline-Formeln

Wenn eine Formel direkt im laufenden Text erscheinen soll, so verwendet man eine Inline-Formel, welche in `$`-Zeichen gesetzt wird.

::: example

``` latex
Die Formel von Pythagoras lautet $c^2 = a^2 + b^2$.
```

Die Formel von Pythagoras lautet $c^2 = a^2 + b^2$.
:::

Die Verwendung der Dollarzeichen ist eine Abkürzung für die `math`-Umgebung:

``` latex
Die Formel von Pythagoras lautet
\begin{math}c^2 = a^2 + b^2\end{math}.
```

> Die bekannte Formel von Pythagoras, $c^2 = a^2 + b^2$, bedeutet…

## Die Umgebung `equation`

Die `equation`-Umgebung unterscheidet sich nur dadurch von der `displaymath`-Umgebung, dass die abgesetzten Formeln zusätzlich noch nummeriert werden. Die Formelnummer wird stets rechtsbündig angezeigt:

``` latex
Die Berechnung der Kathete im rechtwinkligen Dreieck zeigt die
Formel~\ref{eq:formel}.

\begin{equation}
c = \sqrt{a^2 + b^2}
\label{eq:formel}
\end{equation}
```

![](./math-1.png)

Selbstverständlich kann man sich im Text auch auf Formeln beziehen. Dies funktioniert genau wie bei Abbildungs- oder Kapitelverweisen, indem man in der `equation`-Umgebung ein Label definiert und dieses im Text referenzieren.

Wenn die Nummerierung weggelassen werden soll, wird die `*`-Variante `equation*` verwendet.

## Die Umgebung `align`

::: warning
Für die Umgebung `align` muss das Paket `amsmath` eingebunden werden.
:::

In der `align`-Umgebung können mehrere Formeln stehen (getrennt durch `\\`, die als Formelgruppe aufgeführt werden. Um Formeln beim Gleichheitszeichen auszurichten, fügt man vor und nach dem Gleichheitszeichen das Tabellen-Trennzeichen `&` ein.

``` latex
\begin{align}
    c^2 &= a^2 + b^2 \\
    c   &= \sqrt{a^2 + b^2}
\end{align}
```

Dieser Code erzeugt den folgende Ausgabe:

![](./math-2.png)

Verwendet man stattdessen die `*`-Variante `align*`, so entfällt die Formelnummerierung.
