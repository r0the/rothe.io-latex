# 6.2 Formeln
---

## Hoch- und Tiefstellung

In mathematischen Formeln kommen häufig Exponenten und Indizes vor. In LaTeX sind auch mehrfache oder sogar gleichzeitige Hoch- und Tiefstellungen problemlos darstellbar. Die Zeichen `^` resp. `_` geben LaTeX an, dass das direkt folgende Zeichen hoch- respektive tiefgestellt werden soll. Um mehrere Zeichen hoch- oder tiefgestellt darzustellen, werden die geschweiften Klammern zur Gruppierung verwendet. Um also die Formel $x_{1,2} = a^2$ zu erzeugen, schreibt man folgenden Code:

``` latex
$x_{1,2} = a^2$
```

Es geht aber natürlich auch komplexer, wie das Beispiel $a^{x^2-1}_1$ zeigt:

``` latex
$a^{x^2-1}_1$
```

::: exercise Aufgabe
Bilde den folgenden Ausdruck nach:
$$ A_{i^{2n}_{n,m}}^{x_1^2+2} $$
***
``` latex
$$ A_{i^{2n}_{n,m}}^{x_1^2+2} $$
```
:::

## Brüche

Für Brücke wird der Befehl `\frac{z}{n}` verwendet. Dabei ist `z` der Zähler und `n` der Nenner.

$$ \frac{a^n}{a^m} = a^{n - m} $$

``` latex
\begin{equation}
    \frac{a^n}{a^m} = a^{n - m}
\end{equation}
```

Im Fliesstext sehen Brüche wie $\frac{a^n}{a^m}$ nicht so toll aus. In dem Fall kann man sie auch mit einem Schrägstrich schreiben: $a^n/a^m$.

## Wurzeln

Wurzeln werden mit dem Befehl `\sqrt{}` eingefügt.

$$ c = \sqrt{a^2 + b^2} $$

``` latex
\begin{equation}
    c = \sqrt{a^2 + b^2}
\end{equation}
```


Man kann auch n-te Wurzeln darstellen mit dem Befehl `\sqrt[n]{}`:

$$ \sqrt[3]{27} = 3 $$

``` latex
\begin{equation}
    \sqrt[3]{27} = 3
\end{equation}
```

::: exercise Aufgabe

Erzeuge den folgenden Text in deinem Dokument. Das Zeichen $\pm$ wird mit dem Befehl `\pm` erzeugt.

Die Lösungsformel der quadratischen Gleichung $ax^2+bx+c=0$ lautet:

$$ x_{1,2}=\frac{-b\pm\sqrt{b^2-4ac}}{2a} $$
:::

## Klammern

In mathematischen Formeln müssen sich die Klammern der Grösse des eingeschlossenen Terms anpassen. Dazu werden die LaTeX-Befehle `\left(` und `\right)` verwendet:

$$\frac{a^n}{b^n} = \left(\frac{a}{b}\right)^n$$

``` latex
\begin{equation}
    \frac{a^n}{b^n} = \left(\frac{a}{b}\right)^n
\end{equation}
```

Für verschiedene Arten von Klammern können die folgenden Befehle verwendet werden:

| Links      | Rechts      | Beispiel                       |
|:---------- |:----------- | ------------------------------:|
| `\left(`   | `\right)`   | $\left(\frac{a}{b}\right)$     |
| `\left[`   | `\right]`   | $\left[\frac{a}{b}\right]$     |
| `\left\{`  | `\right\}`  | $\left\{\frac{a}{b}\right\}$   |
| `\left\|`  | `\right\|`  | $\left\|\frac{a}{b}\right\|$   |
| `\left\\|` | `\right\\|` | $\left\\|\frac{a}{b}\right\\|$ |
| `\langle`  | `\rangle`   | $\langle\frac{a}{b}\rangle$    |
| `\lceil`   | `\rceil`    | $\lceil\frac{a}{b}\rceil$      |
| `\lfloor`  | `\rfloor`   | $\lfloor\frac{a}{b}\rfloor$    |

**Achtung:** Da geschweifte Klammern in LaTeX eine besondere Bedeutung haben, muss ihnen ein Rückstrich vorangestellt werden.

## Summen und Integrale

Das Summenzeichen wird durch den Befehl `\sum`, das Integral durch `\int` erzeugt. Häufig treten bei Summen oder Integralen obere und untere Grenzen auf, diese werden wie bereits bekannt durch `^` resp. `_` direkt hinter dem Summen- oder Integralbefehl erzeugt.

$$ \sum_{i=1}^n\int_a^b $$

Die Schranken werden bei der Summe im abgesetzten Modus anders dargestellt als beim Integral. Um auch beim Integral die gleiche Darstellung zu erhalten, verwendet man den Befehl `\limits`.

``` latex
\sum_{i=1}^n\int\limits_a^b
```

$$ \sum_{i=1}^n\int\limits_a^b $$


::: exercise Aufgabe
Probiere die beiden Befehle (mit oberen und unteren Schranken) innerhalb eines Abschnittes aber auch in einer abgesetzten Umgebung aus. Was stellst du fest?
:::

::: exercise Aufgabe
Bilde die folgenden Formeln exakt nach:

$$ \int\frac{\sqrt{(ax+b)^3}}{x}dx = \frac{\sqrt[2]{(ax+b)^3}}{3}+2b\sqrt{ax+b}+b^2 $$
$$ \int_{-1}^{8}(dx/\sqrt[3]{x}) = \frac{3}{2}(8^{2/3}+1^{2/3}) = 15/2 $$
:::

## Funktionen

Anders als Variablen schreibt man Funktionsnamen in Formeln nicht kursiv. Deshalb müssen für Funktionsnamen Befehle verwendet werden. Hier ein paar Beispiele:

| Funktion  | LaTeX     | Funktion | LaTeX  |
|:--------- |:--------- |:-------- |:------ |
| $\sin$    | `\sin`    | $\exp$   | `\exp` |
| $\cos$    | `\cos`    | $\log$   | `\log` |
| $\tan$    | `\tan`    | $\ln$    | `\ln`  |
| $\arcsin$ | `\arcsin` | $\lim$   | `\lim` |
| $\arccos$ | `\arccos` | $\max$   | `\max` |
| $\arctan$ | `\arctan` | $\min$   | `\min` |
