# 1.2 Overleaf
---

## Bei Overleaf registrieren

Registriere Dich bei Overleaf, indem Du Deine E-Mail-Adresse und ein Passwort eingibst:

* [:link: Overleaf Registrierung][1]


## Editor

Der Editor von Overleaf sieht folgendermassen aus:

![](./overleaf-edit.png)

Links sind alle Dateien des aktuellen Projekts aufgelistet. In der Mitte befindet sich der Editor, wo die ausgewählte Datei bearbeitet werden kann. Rechts wird die Vorschauf des Projekts angezeigt.

Mit der Tastenkombination [Alt]+[S] kann die Vorschau aktualisiert werden.


[1]: https://www.overleaf.com/signup

## Menü

Mit einem Klick auf den _Menu_-Button oben links wird das Menü geöffnet. Hier kann das aktuelle Projekt als Quelltext oder als PDF heruntergeladen werden:

![](./overleaf-download.png)

Ausserdem können hier wichtige Einstellungen vorgenommen werden:

- Bei _Compiler_ sollte __XeLaTeX__ ausgewählt werden. Im Gegensatz zu _pdfLaTeX_ werden damit moderne Schriftarten und somit meher Zeichen unterstützt.

- Bei _Spell check_ sollte __German__ ausgewählt werden.

- Bei _Font Size_ kann die Grösse des Quelltextes den eigenen Bedürfnissen angepasst werden.
