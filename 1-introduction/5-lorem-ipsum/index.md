# 1.5 Lorem Ipsum
---

*Lorem ipsum* ist ein **Blindtext**, der nichts bedeuten soll, sondern als Platzhalter im Layout verwendet wird, um einen Eindruck vom fertigen Dokument zu erhalten. Die Verteilung der Buchstaben und der Wortlängen des pseudo-lateinischen Textes entspricht in etwa der natürlichen lateinischen Sprache. Der Text ist absichtlich unverständlich, damit der Betrachter nicht durch den Inhalt abgelenkt wird.

> Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
>
> Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.

## Lorem Ipsum in LaTeX

In LaTeX kann mit dem Paket `lipsum` ein Blindtext erzeugt werden. Das Paket wird mit dem folgenden Befehl eingebunden:

``` latex
\usepackage{lipsum}
```

Mit dem Befehl `\lipsum` kann nun ein Blindtext erzeugt werden.

``` latex
\lipsum
```

Standardmässig werden sieben Absätze erzeugt. Mit

``` latex
\lipsum[2-10]
```

werden die Absätze 2 bis 10 erzeugt. Insgesamt stehen 150 Absätze zu Verfügung.

Es können auch einzelne Sätze ausgewählt werden. Mit

``` latex
\lipsum[1][2]
```

wird der zweite Satz aus dem ersten Absatz ausgegeben, mit

``` latex
\lipsum[49][1-3]
```

die drei ersten Sätze aus dem 49. Absatz.
