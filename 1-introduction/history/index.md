# Geschichte
---

## TeX

Des Textsatzsystem Tex (sprich: «Tech») wurde ab 1977 von [Donald Knuth][1], einem der bekanntesten Informatiker, entwickelt. Er hat das Programm eigens entwickelt, um sein mehrbändiges Grundlagenwerk *The Art of Computer Programming* am Computer schreiben zu können.



## LaTeX-Compiler

Der ursprüngliche LaTeX-Compiler erzeugt DVI-Dateien, welche heute nicht mehr gebräuchlich sind. Mit der Zeit sind verschiedene Varianten entwickelt worden, um modernere Technologien zu unterstützen.

- **pdfLaTeX** kann aus LaTeX-Quellcode direkt PDF-Dateien erzeugen.
- **XeLaTeX** unterstützt

## KOMA-Script

Die LaTeX-Standardklassen wurden für US-amerikanische typografische Konventionen und Papierformate entwickelt. Für europäische und internationale Bedürfnisse wurden daher die KOMA-Script-Klassen entwickelt, welche sich an entsprechenden Normen (z.B. DIN-Papierformate) orientieren.

| Standardklasse | KOMA-Script-Klasse | Verwendung                                 |
|:-------------- |:------------------ |:------------------------------------------ |
| `article`      | `scrartcl`         | Artikel in einer Fachzeitschrift           |
| `report`       | `scrreprt`         | eigenständige Arbeiten (z.B. Masterarbeit) |
| `book`         | `scrbook`          | Bücher                                     |
| `letter`       | `scrlttr2`         | Briefe                                     |

In diesen Unterlagen werden grundsätzlich die KOMA-Script-Klassen verwendet. Das meiste gilt jedoch auch für die Standardklassen. Was nur für KOMA-Script-Klassen gilt, ist mit einem entsprechenden Vermerk versehen.

[1]: https://de.wikipedia.org/wiki/Donald_E._Knuth
