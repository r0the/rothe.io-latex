# Pakete
---

## Pakete verwenden

Mit Zusatzpaketen kann LaTeX um wichtige Funktionen erweitert werden. Mit dem Befehl `\usepackage{}` kann ein Paket in einem LaTeX-Dokument verwendet werden:

~~~ latex
\usepackage[<mark>Option</mark>]{<mark>Paket</mark>}
~~~

Dabei wird anstelle von `Paket` der Name des Pakets in den geschweiften Klammern angegeben. Manche Pakete benötigen zusätzliche Einstellungen, welche in eckigen Klammern direkt nach dem Befehl angegeben werden, anstelle von `Option`.

## Standardpakete

Die folgenden Pakete sollten in jedem LaTeX-Dokument verwendet werden:

### Paket `babel`

Mit dem Paket `babel` kann das Layout an eine bestimmte Sprache angepasst werden. Beim Laden des Pakets wird als Option die gewünschte Sprache angegeben. Für die neue deutsche Rechtschreibung verwenden wir den folgenden Aufruf:

``` latex
\usepackage[ngerman]{babel}
```

Nun wird die deutsche Silbentrennung verwendet. Zudem werden sämtliche automatisch generierten Texte (wie z.B. «Inhaltsverzeichnis») in Deutsch ausgegeben.

| Babel-Option | Sprache             |
|:------------ |:------------------- |
| `ngerman`    | Deutsch             |
| `american`   | US-Englisch         |
| `british`    | britisches Englisch |
| `french`     | Französisch         |
| `italian`    | Italienisch         |
