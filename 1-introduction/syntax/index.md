# 1.3 Syntax
---

In LaTeX schreibst du eine Textdatei, welche die Struktur und das Layout des Dokuments beschreibt. LaTeX konvertiert diesen Quelltext in ein fertiges Dokument.

Eine miminale LaTeX-Datei sieht folgendermassen aus:

::: columns 2
``` latex
\documentclass{scrartcl}

\begin{document}
\Large Hallo Welt!
\end{document}
```
***
![](./hello-world.png)
:::

## Leerzeichen

LaTeX ignoriert mehrere Leerzeichen. Ein Zeilenumbruch wird auch als Leerzeichen interpretiert. Ein Absatzwechsel wird durch eine leere Zeile markiert.

::: columns 2
``` latex
Es spielt keine Rolle, ob du
ein oder mehrere     Leerzeichen
nach einem Wort tippst.

Nach einer Leerzeile beginnt ein
neuer Absatz.
```
***
![](./spaces.png)
:::

## Befehle

Ein LaTeX-Befehl beginnt mit einem Rückstrich `\`, gefolgt vom Namen des Befehls. Ein Beispiel ist der Befehl `\Large` im ersten Beispiel.

~~~ latex
\befehlsname
~~~

::: info Rückstrich tippen
Der Rückstrich kann auf Windows-Computern mit der Tastenkombination [AltGr]+[<] getippt werden.

Auf Macs wird die Tastenkombination [Shift]+[Option]+[7] verwendet.
:::


Bei LaTeX-Befehlen ist die Gross- und Kleinschreibung wichtig. Die Befehle `\large`, `\Large` und `\LARGE` haben eine andere Bedeutung, den Befehl `\LArge` gibt es nicht.

Einige Befehle benötigen ein **Argument**, welches in geschweiften Klammern `{}` nach dem Befehl angegeben wird:

~~~ latex
\befehlsname{argument}
~~~

Der Befehl `\documentclass{scrartcl}` im ersten Beispiel ist ein solcher Befehl.

::: info Geschweifte Klammern tippen
Geschweifte Klammern können auf Windows-Computern mit der Tastenkombination [AltGr]+[ä] sowie [AltGr]+[$] getippt werden.

Auf Macs werden die Tastenkombinationen [Option]+[8] und [Option]+[9] verwendet.
:::

Zusätzlich können einem Befehl Optionen übergeben werden. Diese werden zwischen Befehlsname und Argument in eckigen Klammern `[]` angegeben.

~~~ latex
\befehlsname[option1,option2,...]{argument1}
~~~

Beispielsweise kann man dem Befehl `\documentclass` die Optionen `12pt` und `a5paper` übergeben, um die Schriftgrösse auf 12 Punkt festzulegen und das Papierformat auf A5. Das sieht dann so aus:

``` latex
\documentclass[12pt,a5paper]{scrartcl}
```

::: info Eckicke Klammern tippen
Geschweifte Klammern können auf Windows-Computern mit der Tastenkombination [AltGr]+[ü] sowie [AltGr]+[¨] getippt werden.

Auf Macs werden die Tastenkombinationen [Option]+[5] und [Option]+[6] verwendet.
:::

## Kommentare

Wenn LaTeX das Zeichen `%` antrifft, so ignoriert es den Rest der Zeile. So können Kommentare in das Dokument eingefügt werden, welche nicht im Ausgabedokument erscheinen.

::: columns 2
``` latex
Kommentare sind % unnötig
hilfreich.
```
***
![](./comment.png)
:::

## Spezialzeichen

Die folgenden Zeichen haben in LaTeX-Dokumenten eine spezielle Bedeutung:

```
# $ & _ { } ~ ^ \
```

Wenn du diese Zeichen im Ausgabedokument erzeugen möchtest, musst du folgende Befehle benutzen:

| Zeichen | LaTeX-Befehl       |
|:------- |:------------------ |
| `#`     | `\#`               |
| `$`     | `\$`               |
| `&`     | `\&`               |
| `_`     | `\_`               |
| `{`     | `\{`               |
| `}`     | `\}`               |
| `~`     | `\textasciitilde`  |
| `^`     | `\textasciicircum` |
| `\`     | `\textbackslash`   |

## Umgebungen

Mit den speziellen Befehlen `\begin{umgebungsname}` und `\end{umgebungsname}` wird eine **Umgebung** definiert. Eine Umgebung ist ein Bereich des Dokumdents, für welche eine bestimmte Formatierung gilt, beispielsweise eine Aufzählung oder ein Abschnitt mit zwei Spalten.

Im ersten Beispiel wird die Umgebung `document` definiert. Sie umfasst alles, was im Ausgabedokument erscheinen soll.

``` latex
\begin{document}
Inhalt des Dokuments
\end{document}
```

## Gruppen
