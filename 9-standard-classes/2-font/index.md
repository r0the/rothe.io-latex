# Schriftarten
---

### Paket `inputenc`

Mit diesem Paket kann die Zeichencodierung für das LaTeX-Dokument eingestellt werden. Damit ist es unter anderem möglich, Umlaute zu verwenden.

``` latex
\usepackage[utf8]{inputenc}
```


### Paket `fontenc`

Mit diesem Paket kann die Schriftcodierung eingestellt werden. LaTeX verwendet standardmässig die Codierung `OT1` mit welcher nur 128 verschiedene Schriftzeichen dargestellt werden können. Beispielsweise können die Zeichen `<` und `>` so nicht dargestellt werden.

Mit `T1` können bis zu 256 unterschiedliche Schriftzeichen dargestellt werden, es sollte also immer diese Option gewählt werden:

``` latex
\usepackage[T1]{fontenc}
```


## Schriftfamilien

In LaTeX wird zwischen drei Familien von Schriften unterschieden: Schriftarten mit Serifen, serifenfreie Schriftarten, sowie Schreibmaschinenschriften.

| Befehl       | Bezeichnung                                          | Beispiel                                                     |
|:------------ |:---------------------------------------------------- |:------------------------------------------------------------ |
| `\rmdefault` | Standardschriftart mit Serifen (engl. *roman*)       | <span style="font-family: serif;">mit Serifen</span>         |
| `\sfdefault` | serifenfreie Standardschriftart (engl. *sans serif*) | <span style="font-family: sans-serif;">serifenfrei</span>    |
| `\ttdefault` | Schreibmaschinenschrift (engl. *teletypewriter*)     | <span style="font-family: monospace;">Schreibmaschine</span> |

Für normalen Text verwendet LaTeX die Schriftfamilie `\rmdefault`. Mit dem folgenden Befehl kann beispielsweise die Familie `\sfdefault` gewählt werden:

``` latex
\renewcommand{\familydefault}{\sfdefault}
```

## Schriftart Helvetica

Mit diesem Paket kann die Schriftart «Helvetica» als gute Alternative zu Arial verwendet werden. Hier muss zusätzlich noch die serifenfreie Schrift `\sfdefault` als Standardschriftart festgelegt werden.

``` latex
\usepackage{helvet}
\renewcommand{\familydefault}{\sfdefault}
```

## LaTeX-Schriftarten

Im LaTeX Font Catalogue können verfügbaren Schriftarten betrachtet werden:

* [:link: LaTeX Font Catalogue](http://www.tug.org/FontCatalogue/texgyreadventor/)

**Achtung:** Beachte den Abschnitt **Part of TEX Live?**. Nur Schriftarten, die in TEX Live vorhanden sind, können in Overleaf verwendet werden.
