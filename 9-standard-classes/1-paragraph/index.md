# Absatzabstand
---

In LaTeX werden Absätze normalerweise durch einen Einzug hervorgehoben. Mit dem Paket `parskip` kann als Alternative ein vertikaler Abstand definiert werden:

~~~ latex
\usepackage[indent=<mark>Einzug</mark>, skip=<mark>Abstand</mark>]{parskip}
~~~

Dabei kann für `Einzug` ein Wert für den Einzug der ersten Zeile angegeben werden. Für `Abstand` kann ein Wert für den vertikalen Abstand zweier Absätze angegeben werden.

Um keinen Einzug und eine Zeile Abstand zu erhalten, wird folgender Befehl verwendet:

``` latex
\usepackage[skip=10pt]{parskip}
```
