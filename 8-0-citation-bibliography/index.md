# Zitate und Bibliografie
---

Eine grosse Stärke von LaTeX ist die Automation des Zitierens und Bibliografierens.

## Grundlagen

### Quellenangabe

In einer wissenschaftlichen Arbeit müssen sämtliche Aussagen belegt (zitiert) werden. Beim Zitieren wird im Text einen Hinweis auf die Quelle der Aussage gemacht:

> Gemäss **Seifert (2018, S. 11-12)** hilft die Visualisierung, den Fokus des Zuhörers auf das Wesentliche zu richten.

Die Quellenangabe kann auch in Klammern am Schluss des Satzes stehen:

> Visualisierung hilft, den Fokus des Zuhörers auf das Wesentliche zu richten **(Seifert, 2018, S. 11-12)**.

Die Schreibweise der Quellenangabe ist je nach Fachgebiet und Institution klar reglementiert. Eine solche vorgegebene Schreibweise wird *Zitierstil* genannt.

### Bibliografie

Quellenangeben enthalten nicht die vollständige Information über die Quelle, es sind Verweise auf die Bibliografie. In der Bibliografie werden sämtliche verwendeten Quellen vollständig aufgeführt.

> Seifert, Josef W. (2018). *Visualisieren, Präsentieren, Moderieren* (40. Aufl.). Offenbach: GABAL Verlag GmbH.

Auch für die Gestaltung der Bibliografie gibt es klare Regeln. Hier spricht man vom *Bibliografiestil*.

## Zitate und Bibliografie in LaTeX

Um in LaTeX Zitate und Bibliografie einfach nutzen zu können, müssen erst die Pakete `csquotes` und `biblatex` eingebunden werden:

``` latex
\usepackage{csquotes}
\usepackage[style=authoryear]{biblatex}
```
Mit dem Parameter `style` wird der Bibliografiestil ausgewählt, siehe unten.

Als nächstes muss die Datei angegeben werden, in welcher die Bibliografie-Datenbank definiert ist.

``` latex
\addbibresource{literatur.bib}
```

Die Datei enthält für jedes verwendete Werk einen Eintrag in der folgenden Art:

``` latex
@book{Seifert,
  author = {Seifert, Josef W.},
  title = { {Visualisieren, Präsentieren, Moderieren} },
  publisher = {GABAL Verlag GmbH},
  address = {Offenbach},
  year = {2018},
  edition = {40}
}
```

Nun können die folgenden Befehle verwendet werden, um eine Quellenangabe im Text einzufügen:

~~~ latex
\textcite[Seiten]{Werk}
~~~
fügt eine Quellenangabe auf das angegebene Werk direkt im Text ein. Beispiel:

``` latex
Gemäss \textcite[S. 11-12]{Seifert} hilft die Visualisierung,
den Fokus des Zuhörers auf das Wesentliche zu richten.
```

~~~ latex
\parencite[Seiten]{Werk}
~~~
fügt eine Quellenangabe auf das angegebene Werk in Klammern ein. Beispiel:

``` latex
Visualisierung hilft, den Fokus des Zuhörers auf das Wesentliche
zu richten \parencite[S. 11-12]{Seifert}.
```

Mit dem Befehl

~~~ latex
\printbibliography
~~~
wird die Bibliografie erzeugt.

## Bibliographie

Die Bibliographie wird in einer separaten Datei mit der Dateiendung **.bib** abgelegt.


## Bibliografierstil

`biblatex` unterstützt viele bekannte Bibliografierstile, unter anderem folgende:

| Stil          | Beschreibung                                             |
|:------------- |:-------------------------------------------------------- |
| `numeric`     | Referenzmethode (Vancouvermethode)                       |
| `alphabetic`  | alphabetischer Stil                                      |
| `authoryear`  | Autor-Jahr-Methode (Harvard)                             |
| `authortitle` | Autor-Titel-Methode                                      |
| `apa`         | American Psychological Association (APA)                 |
| `chem-acs`    | American Chemical Society (ACS)                          |
| `ieee`        | Institute of Electrical and Electronics Engineers (IEEE) |
| `phys`        | American Institute of Physics (AIP)                      |

## Bibliografie-Datenbank

Im folgenden wir der Aufbau einer Bibtex-Datei erklärt. Die Datei enthält aufeinanderfolgende **Einträge**. Jeder Eintrag beginnt mit einem `@`-Zeichen, gefolgt vom **Typ** des Eintrags. Danach folgt in geschweiften Klammern der eigentliche Eintrag:

```
@typ{Referenz1,
  feld1 = {wert1},
  feld2 = {wert2}
}
@typ{Referenz2,
  feld1 = {wert1},
  feld2 = {wert2}
}
```

Die wichtigsten Typen für Einträge sind folgende:

| Typ           | Bedeutung                          |
|:------------- |:---------------------------------- |
| `book`        | Buch                               |
| `article`     | Artikel, z.B. in einer Zeitschrift |
| `online`      | Online-Quelle                      |

Die **Referenz** wird verwendet, um im Text auf den entsprechenden Eintrag zu verweisen. Die Referenz kann selbst gewählt werden.

Die verschiedenen Angaben zu einem Eintrag werden mit der Syntax `feld = {wert}` angegeben. Die folgende Tabelle zeigt die wichtigsten Felder:

| Feld           | Bedeutung                                     |
|:-------------- |:--------------------------------------------- |
| `author`       | Autor(en)                                     |
| `title`        | Titel des Werks                               |
| `publisher`    | Verlag                                        |
| `address`      | Verlagsort                                    |
| `year`         | Publikationsjahr                              |
| `edition`      | Auflage                                       |
| `url`          | Link (z.B. bei Webseiten)                     |
| `urldate`      | Abrufdatum für den Link                       |
| `journaltitle` | Titel der Zeitschrift bei Artikeln            |
| `number`       | Nummer der Zeitschrift                        |
| `pages`        | Seitenzahlen, z.B. bei Artikel in Zeitschrift |
| `volume`       | Band bei einem mehrbändigen Werk              |

Die Werte sollten immer in geschweiften Klammern angegeben werden. Ansonsten werden die Werte möglicherweise von LaTeX verändert.

Autoren werden immer nach dem Schema «Nachname, Vorname» angegeben. Mehrere Autoren werden durch «and» getrennt.

### Bücher

Bei Büchern muss mindestens der Autor, Titel und Jahr angegeben werden. Ausserdem empfiehlt sich die Angabe von Verlag, Verlagsort und Edition:

``` latex
@book{Malorny2007,
  author = {Malorny, Christian and Langner, Marc Alexander},
  title = { {Moderationstechniken: Werkzeuge für die Teamarbeit} },
  publisher = {Carl Hansner Verlag},
  address = {München},
  year = {2007},
  edition = {3}
}
```

### Artikel

Bei Artikel ist zusätzlich das Feld `journaltitel` obligatorisch. Hier ein typisches Beispiel:
``` latex
@article{Wettstein,
  author  = {Wettstein, Alexander and Ramseier, Erich and Scherzinger, Marion and Gasser, Luciano},
  title = { {Unterrichtsstörungen aus Lehrer- und Schülersicht} },
  journaltitle = {Zeitschrift für Entwicklungspsychologie und Pädagogische Psychologie},
  year = 2016,
  number = 48,
  pages = {171-183},
  volume = 4
}
```

### Online-Quelle

Online-Quellen können mit dem Typ `online` angegeben werden. Achtung: Für online verfügbare Artikel oder Bücher sollte `article` oder `book` verwendet werden.

```
@online{Wikipedia:LaTeX,
  author = {Wikipedia},
  title = {LaTeX --- Wikipedia},
  year = {2019},
  url = {https://de.wikipedia.org/wiki/LaTeX},
  urldate = {11. Juni 2019}
}
