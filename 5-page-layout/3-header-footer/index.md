# 5.3 Kopf- und Fusszeile
---

## Standard-Seitenstile

Den Inhalt von Kopf- und Fusszeile wird über den Seitenstil festgelegt. Es werden die folgenden Befehle verwendet:

~~~ latex
\pagestyle{Seitenstil}
~~~
legt den Seitenstil für die aktuelle und alle folgenden Seiten fest.

~~~ latex
\thispagestyle{Seitenstil}
~~~
legt den Seitenstil für die aktuelle Seite fest. Für **Seitenstil** können folgende Werte verwendet werden:

| Seitenstil | Bedeutung                                                            |
|:---------- |:-------------------------------------------------------------------- |
| `empty`    | Kopf- und Fusszeile sind leer                                        |
| `plain`    | Kopfzeile ist leer, Fusszeile enthält in der Mitte die Seitennummer  |
| `headings` | Fusszeile ist leer, Kopfzeile enthält Überschriften und Seitennummer |

![](./pagestyle.svg)

Um beispielsweise eine leere Kopf- und Fusszeile auf der aktuellen Seite zu erhalten, schreibt man:

``` latex
\thispagestyle{empty}
```

## Selbstdefinierte Stile

::: warning Achtung
Dieser Abschnitt gilt nur für KOMA-Script-Klassen (`scr…`). Wenn eine Standardklasse verwendet wird, sollte nicht `scrlayer` verwendet werden, sondern das Paket `fancyhdr`.
:::

Mit dem Zusatzpaket `scrlayer` kann das Aussehen von Kopf- und Fusszeile für KOMA-Script-Klassen selbst definiert werden. Dazu wird wie üblich erst das Paket eingebunden:

``` latex
\usepackage{scrlayer-scrpage}
```

Danach muss der Seitenstil `scrheadings` gesetzt werden:

``` latex
\pagestyle{scrheadings}
```

Nun stehen die folgenden Befehle zu Verfügung, um den linken, mittleren und rechten Bereich von Kopf- und Fusszeile festzulegen:

|                          | links       | zentriert   | rechts      |
|:------------------------ |:----------- |:----------- |:----------- |
| Kopfzeile ungerade Seite | `\lohead{}` | `\cohead{}` | `\rohead{}` |
| Fusszeile ungerade Seite | `\lofoot{}` | `\cofoot{}` | `\rofoot{}` |
| Kopfzeile gerade Seite   | `\lehead{}` | `\cehead{}` | `\rehead{}` |
| Fusszeile gerade Seite   | `\lefoot{}` | `\cefoot{}` | `\refoot{}` |

![](./scrheadings.svg)

Dabei wird zwischen sogenannten geraden und ungeraden Seiten unterschieden. Dies ist jedoch nur für zweiseitiges Layout (Klassenoption `twoside`) von Bedeutung. Bei einem einseitigen Layout gibt es nur **ungerade** Seiten.

In den geschweiften Klammern der Befehle wird jeweils der Text angegeben, welcher an der entsprechenden Position stehen soll. Im Text können natürlich auch Befehle vorkommen. Die folgenden Befehle sind besonders nützlich:

~~~ laxtex
\thepage
~~~
gibt die aktuelle Seitennummer aus.

~~~ laxtex
\thechapter
~~~
gibt die Nummer des aktuellen Kapitels aus.

~~~ laxtex
\thesection
~~~
 gibt die Nummer des aktuellen Abschnitts aus.

~~~ laxtex
\leftmark
~~~
gibt aktuelle Nummer und Titel der obersten Gliederungsebene aus.

~~~ laxtex
\rightmark
~~~
gibt aktuelle Nummer und Titel der zweitobersten Gliederungsebene aus.

## Anzahl Seiten ausgeben

Um die Seitenzahl in der Form «Seite 1 von 5» anzugeben, muss die Gesamtzahl der Seiten ermittelt werden. Dazu wird das Paket `lastpage` verwendet.

``` latex
\usepackage{lastpage}
```

Nun kann mit dem Befehl

~~~ latex
\pageref{LastPage}
~~~
die gesamte Anzahl Seiten ermittelt werden. Die gewünschte Ausgabe «Seite 1 von 5» erhält man mit dem folgenden Befehl:

``` latex
Seite \thepage{} von \pageref{LastPage}
```

## Vollständiges Beispiel

``` latex
\documentclass{scrreprt}
\usepackage[ngerman]{babel}
\usepackage[utf8]{inputenc}
\usepackage{scrlayer-scrpage}

\pagestyle{scrheadings}
\rohead{Beispiel}
\lohead{Stefan Rothe}
\cofoot{} % Standard löschen
\rofoot{Seite \thepage}

\begin{document}
...
\end{document}
```

### Trennstriche

Um unterhalb der Kopfzeile beziehungsweise oberhalb der Fusszeile einen Trennstrich zu erzeugen, können die folgenden Befehle verwendet werden:

~~~ latex
\KOMAoption{headsepline}{Status}
~~~
schaltet den Trennstrich unterhalb der Kopfzeile ein (**Status** `on`) oder aus (`off`).

~~~ latex
\KOMAoption{footsepline}{Status}
~~~
schaltet den Trennstrich unterhalb der Kopfzeile ein (**Status** `on`) oder aus (`off`).
