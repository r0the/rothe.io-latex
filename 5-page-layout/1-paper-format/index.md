# 5.1 Papierformat
---


- `a4paper`, `a5paper`, `letterpaper`: Diese drei – sowie einige weitere Papierformate – können gewählt werden, wenn der Standard nicht dem Wunsch entspricht.
