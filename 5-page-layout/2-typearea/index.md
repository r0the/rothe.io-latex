# 5.2 Seiteneinteilung
---

::: warning Achtung
Dieser Abschnitt gilt nur für KOMA-Script-Klassen (`scr…`).
:::

In der Typografie wird der Bereich einer Seite, in welcher der Textkörper zu liegen kommt, als **Satzspiegel** bezeichnet. Der Position und Grösse des Satzspiegels kommt eine wichtige Bedeutung zu. Die geometrischen Verhältnisse zwischen Satzspiegel und Rändern bestimmen, wie harmonisch die Gestaltung der Seite wirkt.

Ein klassischer Ansatz ist die sogenannte **Neunerteilung**. Die Seite wird in ein 9×9-Gitter aufgeteilt. Die oberste Zeile wird für den Kopfbereich reserviert, die untersten zwei für den Fussbereich. Seitlich bildet eine Spalte für den inneren Rand und zwei Spalten den äusseren Rand. Dadurch hat der Satzspiegel das gleiche Seitenverhältnis wie die gesamte Seite und ist harmonisch platziert.

![Seiteneinteilung mit einem 9×9-Gitter](./typearea.svg)

Die KOMA-Script-Klassen verwenden diesen Ansatz. Die Anzahl Zeilen und Spalten des Gitters können jedoch variiert werden. So können grössere oder kleinere Ränder erzeugt werden. Mit dem Befehl

~~~ latex
\KOMAoption{DIV}{n}
~~~
wird festgelegt, dass das Gitter aus **n** Zeilen und Spalten bestehen soll.

Die folgende Tabelle zeigt die Auswirkungen auf die Ränder für verschiedene Zahlen für **n**:

| n   | Rand oben | Rand innen | Rand einseitig |
|:--- | ---------:| ----------:| --------------:|
| 6   |     49.50 |      35.00 |          52.50 |
| 7   |     42.43 |      30.00 |          45.00 |
| 8   |     37.13 |      26.25 |          39.37 |
| 9   |     33.00 |      23.33 |          35.00 |
| 10  |     29.70 |      21.00 |          31.50 |
| 11  |     27.00 |      19.09 |          28.63 |
| 12  |     24.75 |      17.50 |          26.25 |
| 13  |     22.85 |      16.15 |          24.22 |
| 14  |     21.21 |      15.00 |          22.50 |
| 15  |     19.80 |      14.00 |          21.00 |

Um einen möglichst kleinen Rand zu erhalten, verwendet man den Befehl

``` latex
\KOMAoption{DIV}{15}
```

## Einseitig oder Zweiseitig

Klassischerweise wird bei Büchern zwischen der linken und rechten Seite unterschieden, die Seiten sind nicht symmetrisch aufgebaut. Für andere Anwendungen (z.B. PDFs) kann aber auch ein einseitiges Layout gewählt werden, bei welchem linker und rechter Seitenrand gleich gross sind.

Normalerweise sind die Dokumentklassen `scrreprt` und `scrbook` zweiseitig. Um sie mit einem einseitigen Layout zu verwenden, benutzt man die Option `oneside`:

``` latex
\documentclass[oneside]{scrreprt}
```

Die Dokumentklasse `scrartcl` ist hingegen standardmässig einseitig. Um hier ein zweiseitiges Layout zu erzwingen, benutzt man die Option `twoside`:

``` latex
\documentclass[twoside]{scrartcl}
```
