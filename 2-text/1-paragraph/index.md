# 2.1 Absätze
---

Dies hier ist ein Absatz. Ein Absatz umfasst meist nur einen Gedankengang, ein Argument oder ein kleines Thema. Mehrere verschiedene Gedankengänge, Argumente oder Themen werden in der Regel in mehrere Absätze unterteilt.

Zwei Absätze werden in LaTeX durch eine leere Zeile getrennt:

``` latex
Dies ist der erste Absatz.
Hier ist immer noch der erste Absatz.

Dies ist der zweite Absatz.
```

## Optische Hervorhebung[^1]

Im traditionellen deutschen Schriftsatz erhält jeder Absatz einen **Einzug**. Ein Einzug ist der Leerraum zu Beginn der ersten Zeile eines Absatzes.

::: columns 2
![Darstellung mit Einzug](./paragraph-1.png)
***
![Darstellung mit Leerzeile](./paragraph-2.png)
:::

In amerikanischen Geschäftspapieren ist Einzug nicht üblich, die Absätze werden nur durch **Leerzeilen** hervorgehoben. Dies geht historisch auf die Nutzung mechanischer Schreibmaschinen zur Erstellung solcher Dokumente zurück.

Diese Darstellung wurde in den ersten Webbrowsern und Textverarbeitungsprogrammen wie Word übernommen und hat sich daher als eine Art Welt-Standard herausgebildet.

## Absatzabstand in LaTeX

::: warning
Dieser Abschnitt bezieht sich auf KOMA-Dokumentklassen.
:::

In LaTeX werden Absätze normalerweise durch einen Einzug hervorgehoben. Beim `\documentclass`-Befehl kann mit dem Parameter `parskip` anstelle des Einzuges ein vertikaler Abstand zur Trennung von Absätzen verlangt werden:

Mit `parskip=full` werden die Absätze durch eine ganze Leerzeile getrennt:
``` latex
\documentclass[parskip=full]{scrartcl}
```

Mit `parsktip=half` werden die Absätze durch eine halbe Leerzeile getrennt:
``` latex
\documentclass[parskip=half]{scrartcl}
```

## Manueller Abstand

Um einen manuellen Abstand zwischen zwei bestimmten Absätzen einzufügen, wird der Befehl `\vspace` verwendet:

``` latex
\vspace{10pt}
```


[^1]: Quelle: [Wikipedia - Absatz (Text)](https://de.wikipedia.org/wiki/Absatz_(Text))
