# 2.3 Zeichenformatierung
---

~~~ latex
\emph{Text}
~~~
kennzeichnet den **Text** als hervorgehoben. Hervorgehobener Text wird üblicherweise in kursiver Schrift dargestellt.

~~~ latex
\texttt{Text}
~~~
stellt den **Text** in Schreibmaschinenschrift dar.

~~~ latex
\textbf{Text}
~~~
stellt den **Text** fett dar (*bf* steht für *bold face*).

~~~ latex
\textit{Text}
~~~
stellt den **Text** kursiv dar (*it* steht für *italic*).

~~~ latex
\textsuperscript{Text}
~~~
stellt den **Text** hochgestellt dar.

~~~ latex
\textsubscript{Text}
~~~
stellt den **Text** tiefgestellt dar.
