# 2.5 Schriftgrösse
---

::: warning
Dieser Abschnitt bezieht sich auf KOMA-Dokumentklassen.
:::

Die Schriftgrösse kann über den optionalen Parameter `fontsize` des Befehls `\documentclass` eingestellt werden. Beispielsweise wird mit

``` latex
\documentcass[fontsize=15pt]{scrartcl}
```
eine Schriftgrösse von 15pt eingestellt. Voreingestellt ist eine Schriftgrösse von 11pt.

## Schriftgrösse ändern

Mit den folgenden Befehlen wird die Schriftgrösse ab der Position des Befehls geändert:

| Befehl          | Beispiel                |
|:--------------- |:----------------------- |
| `\tiny`         | ![](./tiny.png)         |
| `\scriptsize`   | ![](./scriptsize.png)   |
| `\footnotesize` | ![](./footnotesize.png) |
| `\small`        | ![](./small.png)        |
| `\normalsize`   | ![](./normalsize.png)   |
| `\large`        | ![](./large-1.png)      |
| `\Large`        | ![](./large-2.png)      |
| `\LARGE`        | ![](./large-3.png)      |
| `\huge`         | ![](./huge-1.png)       |
| `\Huge`         | ![](./huge-2.png)       |
