# 2.4 Aufzählungen
---

Üblicherweise wird den einzelnen Punkten einer Aufzählung ein **Aufzählungszeichen** vorangestellt. In LaTeX wird zwischen nummerierten Aufzählungen und nicht nummerierten Aufzählungen unterschieden.

## Nummerierte Aufzählungen

Eine nummerierte Aufzählung wird in LaTeX mit `\begin{enumerate}` eingeleitet und mit `\end{enumerate}` abgeschlossen. Die einzelnen Punkte der Aufzählung werden mit `\item` eingeleitet:

``` latex
Hier ist eine Aufzählung:
\begin{enumerate}
  \item Erstens
  \item Zweitens
  \item und Drittens
\end{enumerate}
```

LaTeX stellt diese Aufzählung so dar:

![](./enumerate.png)

## Nicht nummerierte Aufzählungen

Eine nicht nummerierte Aufzählung wird in LaTeX mit `\begin{itemize}` eingeleitet und mit `\end{itemize}` abgeschlossen. Die einzelnen Punkte der Aufzählung werden mit `\item` eingeleitet:

``` latex
Hier ist eine Aufzählung:
\begin{itemize}
  \item Erstens
  \item Zweitens
  \item und Drittens
\end{itemize}
```

LaTeX stellt diese Aufzählung wie folgt dar:

![](./itemize.png)

## Paket `enumitem`

Mit dem Paket `enumitem` kann das Aussehen von Aufzählungen angepasst werden. Dazu muss erst das Paket im Vorspann eingebunden werden:

``` latex
\usepackage{enumitem}
```

Nun kann beim Befehl `\begin{enumerate}` mit einem optionalen Parameter die Art der Nummerierung angepasst werden:

``` latex
\begin{enumerate}[label=(\roman*)]
\item Erstens
\item Zweitens
\item und Drittens
\end{enumerate}
```

Es stehen folgende Varianten zu Verfügung:

| Option       | Nummerierung   |
|:------------ |:-------------- |
| `(\roman*)`  | i, ii, iii, iv |
| `(\Roman*)`  | I, II, III, IV |
| `(\arabic*)` | 1, 2, 3        |
| `(\alph*)`   | a, b, c        |
| `(\Alph*)`   | A, B, C        |

Auch für nicht nummerierte Aufzählungen kann das Aufzählungszeichen angepasst werden. Hier wird einfach das gewünschte Zeichen als Wert für `label` angegeben. Im folgenden Beispiel wird mit `--` ein Gedankenstrich als Aufzählungszeichen festgelegt:

``` latex
\begin{itemize}[label=--]
  \item Erstens
  \item Zweitens
  \item und Drittens
\end{itemize}
```

## Abstände anpassen

Mit dem `enumitem`-Paket können mit den folgenden Werten die vertikalen Abstände vor, nach und zwischen den einzelnen Aufzählungspunkten angepasst werden:

| Wert      | Bedeutung                                                  |
|:----------|:---------------------------------------------------------- |
| `parsep`  | Absatzabstand in der Aufzählung                            |
| `itemsep` | Abstand zwischen Aufzählungspunkten                        |
| `topsep`  | zusätzlicher Abstand zwischen normalem Text und Aufzählung |

Diese Abstände können für jede Liste einzeln festgelegt werden, indem sie als optionale Argumente übergeben werden. Mit dem folgenden Code wird eine Liste ohne Abstände zwischen den Aufzählungspunkten erzeugt:

``` latex
\begin{itemize}[parsep=0pt, itemsep=0pt]
\end{itemize}
```

Mit dem Befehl `\setlist{}` können die Werte aber auch für alle Listen im Dokument festgelegt werden:

``` latex
\setlist{topsep=-\parskip, parsep=0pt, itemsep=0pt}
```

Auch wenn `topsep` auf `0pt` gesetzt wird, wird zwischen normalem Text und der Liste der normale Absatzabstand eingefügt. Um diesen zu reduzieren kann für `topsep` ein negativer Wert angegeben werden. Mit `topsep=-\parskip` wird der Absatzabstand ganz aufgehoben:

``` latex
Hier ist eine Aufzählung:

\begin{itemize}[topsep=-\parskip, parsep=0pt, itemsep=0pt]
  \item Erstens
  \item Zweitens
  \item und Drittens
\end{itemize}

Das war die Aufzählung.
```

Das sieht so aus:

![](./enumitem-2.png)
