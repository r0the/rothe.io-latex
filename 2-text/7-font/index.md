# 2.6 Schrifarten
---

## Schriftfamilien

In LaTeX wird zwischen drei Familien von Schriften unterschieden: Schriftarten mit Serifen, serifenfreie Schriftarten, sowie Schreibmaschinenschriften.

| Schalter    | Befehl      | Bezeichnung                                          | Beispiel            |
|:----------- |:----------- |:---------------------------------------------------- |:------------------- |
| `\rmfamily` | `\textrm{}` | Standardschriftart mit Serifen (engl. *roman*)       | ![](./rmfamily.png) |
| `\sffamily` | `\textsf{}` | serifenfreie Standardschriftart (engl. *sans serif*) | ![](./sffamily.png) |
| `\ttfamily` | `\texttt{}` | Schreibmaschinenschrift (engl. *teletypewriter*)     | ![](./ttfamily.png) |

## Standardschriftfamilie ändern

Um im Dokument die serifenfreie Schriftfamilie als Standard zu verwenden, sollte der folgende Befehl verwendet werden:

``` latex
\renewcommand{\familydefault}{\sfdefault}
```

## Schriftarten festlegen

::: warning
Der folgende Abschnit gilt nur für XeLaTeX.
:::

Mit dem XeLaTeX-Paket `fontspec` können Schriftarten festgelegt werden:

``` latex
\usepackage{fontspec}
```

Nun können mit den folgenden drei Befehlen Schriftarten für jede Familie gewählt werden:

| Befehl           | Bedeutung                            |
|:---------------- |:------------------------------------ |
| `\setmainfont{}` | Legt Serifen-Schriftart fest         |
| `\setsansfont{}` | Legt serifenfreie Schriftart fest    |
| `\setmonofont{}` | Legt Schreibmaschinenschriftart fest |

Um die Hauptschriftart in Overleaf zu ändern, lädt man die Schriftart in das Overleaf-Projekt hoch und schreibt:

``` latex
\setmainfont{FontName.ttf}
```
