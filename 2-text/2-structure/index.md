# 2.2 Textgliederung
---

## Überschriften

Überschriften werden mit den folgenden Befehlen definiert:

| Befehl             | Beschreibung        | Ebene |
|:------------------ |:------------------- | -----:|
| `\part{}`          | Teil                |    -1 |
| `\chapter{}`       | Kapitel             |     0 |
| `\section{}`       | Abschnitt           |     1 |
| `\subsection{}`    | Unterabschnitt      |     2 |
| `\subsubsection{}` | Unterunterabschnitt |     3 |

Die Ebene «Kapitel» kann nur in Büchern (`scrbook`) und Berichten (`scrrprt`) verwendet werden, die anderen Ebenen in allen Arten Dokumententypen (ausser Briefen). Der Text der Überschrift wird in den geschweiften Klammern angegeben:

``` latex
\section{Die Überschrift}
```

Um eine Überschrift aus dem Inhaltsverzeichnis auszuschliessen, kann nach dem Befehl ein Sternchen `*` geschrieben werden:

``` latex
\section*{Erscheint nicht im Inhaltsverzeichnis}
```

## Inhaltsverzeichnis

Das Inhaltsverzeichnis wird von LaTeX (fast) automatisch generiert. Mit dem Befehl `\tableofcontents` wird an der entsprechenden Stelle ein Inhaltsverzeichnis aus allen im Dokument definierten Überschriften zusammengestellt.

``` latex
\tableofcontents
```
