# Textformatierung
---

* [2.1 Absätze](?page=1-paragraph/)
* [2.2 Textgliederung](?page=2-structure/)
* [2.3 Zeichenformatierung](?page=3-inline-format/)
* [2.4 Aufzählungen](?page=4-itemize/)
* [2.5 Schriftgrösse](?page=5-font-size/)
* [2.6 Zeilenabstand](?page=6-line-spacing/)
* [2.7 Schriftarten](?page=7-font/)
