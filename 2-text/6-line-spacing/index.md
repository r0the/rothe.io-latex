# Zeilenabstand
---

Bei Texten, welche korrigiert werden, wird oft ein grösserer Zeilenabstand verlangt. Das Paket `setspace` stellt drei Befehle zu Verfügung, mit welchen der Zeilenabstand kontrolliert werden kann. Das Paket wird wie üblich im Kopf des Dokuments eingebunden:

``` latex
\usepackage{setspace}
```

Das Paket stellt die folgenden drei Befehle zu Verfügung:

| Befehl            | Auswirkung                     | Faktor |
|:----------------- |:------------------------------ | ------:|
| `\singlespacing`  | einfacher Zeilenabstand        |     ×1 |
| `\onehalfspacing` | eineinhalbfacher Zeilenabstand |   ×1.5 |
| `\doublespacing`  | doppelter Zeilenabstand        |     ×2 |

Das sieht so aus:

::: columns 3
![einfacher Zeilenabstand](./singlespacing.png)
***

![eineinhalbfacher Zeilenabstand](./onehalfspacing.png)

***
![doppelter Zeilenabstand](./doublespacing.png)
:::
